import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {}

  CompleteSignup(data){
    return this.http.post('user/CompleteSignup', data);
  }

  isSocailUserExist(data){
    return this.http.post('user/isSocailUserExist', data);
  }

  getUserProfile(){
    return this.http.get('user/getUserProfile');
  }

}
