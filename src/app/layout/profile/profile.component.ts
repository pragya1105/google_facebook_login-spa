import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  
  constructor(private utils: UtilsService ,  private api: HttpService,private router: Router) {
    
  }

  ngOnInit() {
      this.getUserDetail()
 }

   User
   getUserDetail(){
     this.api.getUserProfile().subscribe(
      data=> {
        if(data['response']){
           this.User = data['response'] 
        }
      },
      error=> {
        this.utils.alert('error', error['error']['message']);
      }
    )};

    logout(){
      this.utils.setCookie('token','')
      this.router.navigate(['']);
    }

}
