import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { ProfileComponent } from './layout/profile/profile.component';
import { AuthGuard } from './shared/services/auth.guard.service';


const routes: Routes = [
  {path: '',  redirectTo : '/login' , pathMatch :'full'},
  {path: 'login', component:LoginComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
